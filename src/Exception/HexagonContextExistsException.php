<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Exception;

use RuntimeException;

class HexagonContextExistsException extends RuntimeException
{

    public function __construct(string $contextFolder)
    {
        parent::__construct(
            sprintf('The context folder "%s" already exists!!!',$contextFolder)
        );
    }

}