<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Exception;

use RuntimeException;

class HexagonContextDoesNotExistsException extends RuntimeException
{

    public function __construct(string $contextFolder)
    {
        parent::__construct(
            sprintf('The context folder "%s" does not exists!!!',$contextFolder)
        );
    }

}