<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Command;

use Insidesuki\Bundle\HexagonBundle\Service\Structure\CreateHexagonStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Command for create context structure.
 */
class CreateStructureCommand extends Command
{

    protected static $defaultName = 'hexagon:create-structure';
    protected static $defaultDescription = ' Create all hexagon directories';

    protected function configure():void
    {

        $this->addArgument('context',InputArgument::REQUIRED,'Context name is required');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output):int
    {

        $context = $input->getArgument('context');

        $ui = new SymfonyStyle($input,$output);
        $ui->title('InsideSukiBundle-HexagonStructureCreator');

        $ui->section('--Creating:'.$context.' context structure.');

        // create hexagon structure
        $createContext = new CreateHexagonStructure();
        $createContext($input->getArgument('context'));

        $ui->success('Context created!!');
        $ui->info('Please check folder!!');
        return Command::SUCCESS;

    }

}