<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Service\Creator;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use RuntimeException;

class EntityMapping
{
    private AttributeMapping $idAttribute;
    private string $name;
    private string $namespace;
    private string $contextNamespace;
    private string $basePath;
    private string $fullPath;
    private string $entityPath;
    private string $exceptionNamespace;
    private string $entityNamespace;
    private string $exceptionExistsFilename;


    private ArrayCollection $attributes;


    public function __construct(AttributeMapping $idName,
                                string           $namespace,
                                string           $basePath
    )
    {

        $this->idAttribute = $idName;
        $this->setNamespaces($namespace);
        $this->name();
        $this->setPaths($basePath);

        $this->attributes = new ArrayCollection();
    }

    private function setNamespaces(string $namespace)
    {

        $this->namespace = $namespace;
        $namespaceArray = explode('\\', $this->namespace);
        $this->contextNamespace = 'Application\\' . $namespaceArray[1];
        $this->entityNamespace = $this->contextNamespace . '\\Domain\\Model';
        $this->exceptionNamespace = $this->contextNamespace . '\\Domain\\Exception';


    }

    /**
     * @return string
     */
    public function exceptionNamespace(): string
    {
        return $this->exceptionNamespace;
    }


    public function name()
    {
        $namespaceArray = explode('\\', $this->namespace);
        $this->name = end($namespaceArray);

    }

    /**
     * @return string
     */
    public function exceptionExistsFilename(string $type): string
    {

        $exceptionType = ucfirst(strtolower($type));
        return $this->exceptionExistsFilename . '/' . $this->name . $exceptionType . 'ExistsException.php';
    }


    /**
     * Set entity path
     * @param $path
     * @return void
     */
    public function setPaths($path)
    {

        $basePathEntity = realpath($path);
        if (!$basePathEntity) {
            throw new RuntimeException(sprintf('Base path:"%s" for Entity does not exists!!', $path));
        }


        $this->basePath = $basePathEntity;
        $this->entityPath = $basePathEntity . '/Domain/Model';
        $this->fullPath = $this->entityPath . '/' . $this->name . '.php';
        $this->exceptionExistsFilename = $this->basePath . '/Domain/Exception';


    }

    /**
     * @param AttributeMapping $attribute
     * @return void
     */
    public function addAttr(AttributeMapping $attribute)
    {
        if (!($this->attributes->contains($attribute))) {
            $this->attributes->add($attribute);
        }
    }


    public function getIdAttribute(): AttributeMapping
    {
        return $this->idAttribute;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return Collection|ArrayCollection
     */
    public function getAttributes(): Collection|ArrayCollection
    {
        return $this->attributes;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @return false|string
     */
    public function getBasePath(): bool|string
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getFullPath(): string
    {
        return $this->fullPath;
    }

    /**
     * @return string
     */
    public function getEntityPath(): string
    {
        return $this->entityPath;
    }

    /*
     *
     */

    public function getContextNamespace(): string
    {
        return $this->contextNamespace;
    }

    /**
     * @return string
     */
    public function getEntityNamespace(): string
    {
        return $this->entityNamespace;
    }


}