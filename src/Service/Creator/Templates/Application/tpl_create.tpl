<?php
declare(strict_types=1);
namespace [{namespace_service}];

use [{namespace_entity}];
use [{namespace_interface}];
use [{namespace_exception}];

class Create[{entity}]
{


    private [{entity}]Repository  $[{entity_lower}]Repository;

    public function __construct([{entity}]Repository $[{entity_lower}]Repository)
    {
        $this->[{entity_lower}]Repository = $[{entity_lower}]Repository;
    }

    public function handle(): [{entity}]
    {


        $[{entity_lower}] = new [{entity}]();

        $this->[{entity_lower}]Repository->save($[{entity_lower}]);

        return $[{entity_lower}];
    }

}