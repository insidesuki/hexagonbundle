<?php
declare(strict_types=1);
namespace [{namespace_service}];

use [{namespace_interface}];


class Remove[{entity}]
{


    private [{entity}]Repository  $[{entity_lower}]Repository;

    public function __construct([{entity}]Repository $[{entity_lower}]Repository)
    {
        $this->[{entity_lower}]Repository = $[{entity_lower}]Repository;
    }

    public function handle(string $[{id}]): void
    {


        $[{entity_lower}] = $this->[{entity_lower}]Repository->findByIdOrFail($[{id}]);

        $this->[{entity_lower}]Repository->remove($[{entity_lower}]);


    }

}