<?php
declare(strict_types=1);
namespace [{namespace_service}];

use [{namespace_entity}];
use [{namespace_interface}];

class Modify[{entity}]
{


    private [{entity}]Repository  $[{entity_lower}]Repository;

    public function __construct([{entity}]Repository $[{entity_lower}]Repository)
    {
        $this->[{entity_lower}]Repository = $[{entity_lower}]Repository;
    }

    public function handle(string $[{id}]): [{entity}]
    {


        $[{entity_lower}] = $this->[{entity_lower}]Repository->findByIdOrFail($[{id}]);

        $this->[{entity_lower}]Repository->save($[{entity_lower}]);

        return $[{entity_lower}];
    }

}