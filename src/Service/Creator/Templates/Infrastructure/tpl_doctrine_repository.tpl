<?php
declare(strict_types=1);
namespace [{namespace_repo}];

use Application\Common\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use [{namespace_entity}];
use [{namespace_interface}];
use [{namespace_exception}];

class [{entity}]DoctrineRepository extends AbstractDoctrineRepository implements [{entity}]Repository
{

    protected static function entityClass(): string
        {
            return [{entity}]::class;
        }

    public function findById(string $[{id}]) : ?[{entity}]
    {
        return $this->objectRepository->find($[{id}]);

    }

    public function findByIdOrFail(string $[{id}]) :[{entity}]
    {
        $[{entity_lower}] = $this->findById($[{id}]);

        if($[{entity_lower}] === null){
            throw new [{entity}]NotExistsException($[{id}]);
        }

        return $[{entity_lower}];

    }

    public function save([{entity}] $[{entity_lower}]):[{entity}]
    {
        $this->saveEntity($[{entity_lower}]);
        return $[{entity_lower}];
    }

    public function remove([{entity}] $[{entity_lower}]):void
    {
        $this->removeEntity($[{entity_lower}]);
    }


}