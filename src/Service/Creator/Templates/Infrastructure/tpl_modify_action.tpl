<?php
declare(strict_types=1);
namespace [{namespace_action}];

use Application\Common\Infrastructure\Controller\CommonController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use [{namespace_modify}];

class Modify[{entity}]Action extends CommonController
{
    private [{entity}] $modify[{entity}];

    public function __construct(Modify[{entity}] $modify[{entity}])
    {
        $this->modify[{entity}] = $modify[{entity}];
    }

    public function __invoke(string $[{id}],Request $request):Response
    {

        $this->modify[{entity}]->handle($[{id}]);

        return new Response('[{entity}] Updated');


    }


}