<?php
declare(strict_types=1);
namespace [{namespace_action}];

use Application\Common\Infrastructure\Controller\CommonController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use [{namespace_handler}];

class Create[{entity}]Action extends CommonController
{
    private [{entity}] $create[{entity}];

    public function __construct(Create[{entity}] $create[{entity}])
    {
        $this->create[{entity}] = $create[{entity}];
    }

    public function __invoke(Request $request):Response
    {

        $this->create[{entity}]->handle();

        return new Response('[{entity}] Created',Response::HTTP_CREATED);


    }


}