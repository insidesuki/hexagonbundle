<?php
declare(strict_types=1);
namespace [{namespace_action}];

use Application\Common\Infrastructure\Controller\CommonController;
use Symfony\Component\HttpFoundation\Response;
use [{namespace_remove}];

class Remove[{entity}]Action extends CommonController
{
    private [{entity}] $remove[{entity}];

    public function __construct(Remove[{entity}] $remove[{entity}])
    {
        $this->modify[{entity}] = $remove[{entity}];
    }

    public function __invoke(string $[{id}]):Response
    {

        $this->remove[{entity}]->handle($[{id}]);

        return new Response('[{entity}] Removed');


    }


}