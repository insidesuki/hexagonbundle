<?php
declare(strict_types=1);
namespace [{namespace_repo}];

use [{namespace_entity}];
use [{namespace_interface}];
use [{namespace_exception}];

class Exists[{entity}]Repository  implements [{entity}]Repository
{

   private [{entity}] $[{entity}];

    public function __construct([{entity}] $[{entity_lower}]){
        $this->[{entity}] = $[{entity_lower}];
}


    public function findById(string $[{id}]) : ?[{entity}]
    {
        return $this->[{entity}];

    }

    public function findByIdOrFail(string $[{id}]) :[{entity}]
    {

        return $this->[{entity}];

    }

    public function save([{entity}] $[{entity_lower}]):[{entity}]
    {

        return $[{entity_lower}];
    }

    public function remove([{entity}] $[{entity_lower}]):void
    {

    }


}