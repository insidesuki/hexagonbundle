<?php
declare(strict_types=1);
namespace [{namespace_repo}];

use [{namespace_entity}];
use [{namespace_interface}];
use [{namespace_exception}];

class Empty[{entity}]Repository  implements [{entity}]Repository
{

    protected array $data;


    public function findById(string $[{id}]) : ?[{entity}]
    {
        return null;

    }

    public function findByIdOrFail(string $[{id}]) :[{entity}]
    {

            throw new [{entity}]NotExistsException($[{id}]);

    }

    public function save([{entity}] $[{entity_lower}]):[{entity}]
    {

        return $[{entity_lower}];
    }

    public function remove([{entity}] $[{entity_lower}]):void
    {

    }


}