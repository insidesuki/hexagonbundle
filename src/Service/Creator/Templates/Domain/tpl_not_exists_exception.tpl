<?php
declare(strict_types=1);
namespace [{namespace_exception}];

use Application\Common\Domain\Exception\EntityDoesNotExistsException;

class [{entity}]NotExistsException extends EntityDoesNotExistsException
{
    public function __construct($param)
    {
        parent::__construct(sprintf('[{entity}] with "%s" !!', $param));
    }

}