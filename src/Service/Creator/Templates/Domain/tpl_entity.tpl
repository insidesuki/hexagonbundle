<?php
declare(strict_types=1);
namespace [{namespace_entity}];

use Symfony\Component\Uid\UuidV4;


class [{entity}] {


private string $[{Id}];
[{attributes}]

public function __construct([{dependencies}])
{

    $this->[{Id}]  = UuidV4::v4()->toRfc4122();
    [{constructorSetter}]

}

[{getters}]
}
