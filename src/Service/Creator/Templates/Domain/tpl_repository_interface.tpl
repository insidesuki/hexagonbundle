<?php
declare(strict_types=1);
namespace [{namespace_repo}];

use [{namespace_entity}];

interface [{entity}]Repository
{

    public function findById(string $[{id}]) : ?[{entity}];

    public function findByIdOrFail(string $[{id}]) :[{entity}];

    public function save([{entity}] $[{entity_lower}]):[{entity}];

    public function remove([{entity}] $[{entity_lower}]):void;

}