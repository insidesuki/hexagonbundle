<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Service\Creator;

use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;

class EntityParser
{

    private const ROOT_CONTEXT_PATH = __DIR__ . '/../../../../';
    private const PARTIAL_MAPPING_PATH = '/Infrastructure/Storage/Doctrine/Mappings/';


    private string $context;
    private string $mappingFile;
    private Crawler $crawler;
    private string $contextPath;
    private EntityMapping $entity;

    /**
     * @param string $context
     * @param string $mapping
     * @return EntityMapping
     */
    public function __invoke(string $context, string $mapping): EntityMapping
    {

        $this->context = ucfirst(strtolower($context));
        $this->mappingFile = ucfirst(strtolower($mapping)) . '.orm.xml';

        $this->readMappingXml();

        $entity = $this->crawler->filter('entity')->attr('name');

        // create entityID
        $entityId = $this->createId();

        $this->entity = new EntityMapping($entityId, $entity,$this->contextPath);


        // create entity attributes
        $this->createAttributeCollection();

        return $this->entity;

    }

    /**
     * @return void
     */
    private function readMappingXml()
    {

        $this->contextPath = self::ROOT_CONTEXT_PATH . $this->context;
        $pathXml = realpath($this->contextPath) . self::PARTIAL_MAPPING_PATH . $this->mappingFile;
        $xmlMappingFile = realpath($pathXml);

        if (!$xmlMappingFile) {
            throw new RuntimeException(sprintf('Xml mapping "%s" not found!!',$pathXml));
        }
        $dom = file_get_contents($xmlMappingFile);
        $this->crawler = new Crawler($dom);

    }

    /**
     * @return void
     */
    private function createAttributeCollection()
    {

        $fields = $this->crawler->filter('field');

        foreach ($fields as $field) {
            $c = new Crawler($field);
            $type = $c->filter('field')->attr('type');
            $name = $c->filter('field')->attr('name');
            $nullable = $c->filter('field')->attr('nullable');

            $this->entity->addAttr(AttributeMapping::createAttr($name, $type, $nullable)

            );

        }

    }

    /**
     * @return AttributeMapping
     */
    public function createId(): AttributeMapping
    {
        $id = $this->crawler->filter('id')->attr('name');
        $idType = $this->crawler->filter('id')->attr('type');
        $nullable = $this->crawler->filter('id')->attr('nullable');

        return AttributeMapping::createID($id, $idType);


    }

}