<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Service\Creator;

class AttributeMapping
{
    private string $name;
    private string $type;
    private bool $isID;
    private bool $nullable;

    /**
     * @param string $name
     * @param mixed $type
     * @param bool $isID
     * @param bool $nullable
     */
    private function __construct(string $name, mixed $type, bool $isID = false, mixed $nullable = false)
    {
        $this->name = $name;
        $this->type = ($type === null) ? 'string' : $type;
        $this->isID = $isID;
        $this->nullable = ($nullable === null) ? false : $nullable;
    }

    /**
     * Create attribute
     * @param string $name
     * @param mixed $type
     * @param mixed $nullable
     * @return AttributeMapping
     */
    public static function createAttr(string $name, mixed $type, mixed $nullable):self
    {
        return new self($name, $type, false, $nullable);

    }

    /**
     * Create new ID attribute type
     * @param string $name
     * @param string $type
     * @return AttributeMapping
     */
    public static function createID(string $name, string $type):self
    {
        return new self($name, $type, true, false);

    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType(): mixed
    {
        return ($this->type === 'integer') ? 'int' : $this->type;
    }

    /**
     * @return bool
     */
    public function isID(): bool
    {
        return $this->isID;
    }

    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }
}