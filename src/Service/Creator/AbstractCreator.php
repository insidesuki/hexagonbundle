<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Service\Creator;

use Symfony\Component\Filesystem\Filesystem;
use \RuntimeException;

abstract class AbstractCreator
{

    protected const PATH_TPL = __DIR__ . '/Templates';

    protected EntityMapping $entity;
    protected Filesystem $filesystem;
    protected string|bool $contextPath;
    protected string $domainPath;
    protected string $entityFile;


    public function __construct(EntityMapping $entityMapping)
    {

        $this->entity = $entityMapping;
        $this->filesystem = new Filesystem();

        $this->contextPath = $this->entity->getBasePath();
        $this->domainPath = $this->entity->getEntityPath();
        $this->entityFile = $this->entity->getFullPath();


    }

    protected function check()
    {

        if (!$this->filesystem->exists($this->contextPath)) {
            throw new RuntimeException(sprintf('Context path "%s" does not exists', $this->contextPath));
        }
        if (!$this->filesystem->exists($this->domainPath)) {
            throw new RuntimeException(sprintf('Domain path "%s" does not exists', $this->domainPath));
        }

    }


    /**
     * @param array $content
     * @param string $template
     * @return string
     */
    protected function dumpContent(array $content, string $template): string
    {

        return strtr(file_get_contents(realpath($template)), $content);

    }

    /**
     * @param $path
     * @param bool $recreate
     * @return void
     */
    protected function create($path, bool $recreate = true)
    {

        $path_parts = explode('/', $path);

        // delete if exists
        if ($recreate) {
            $this->filesystem->remove($path);
        }

        // create
        $this->filesystem->touch($path);

    }


}