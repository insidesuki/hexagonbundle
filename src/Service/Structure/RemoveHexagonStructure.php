<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Service\Structure;

use Insidesuki\Bundle\HexagonBundle\Exception\HexagonContextDoesNotExistsException;
use Symfony\Component\Filesystem\Filesystem;

class RemoveHexagonStructure
{


    public function __invoke(string $contextFolder)
    {

        $filesystem = new Filesystem();

        // check if exists contextFolder
        if(false === $filesystem->exists($contextFolder)){
            throw new HexagonContextDoesNotExistsException($contextFolder);
        }

        // remove
        $filesystem->remove($contextFolder);


    }

}