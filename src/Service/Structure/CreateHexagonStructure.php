<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\HexagonBundle\Service\Structure;

use Insidesuki\Bundle\HexagonBundle\Exception\HexagonContextExistsException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Create Hexagon base directories
 */
class CreateHexagonStructure
{

    private string|false $rootApp;
    private string $contextPath;
    private string $contextName;
    private Filesystem $filesystem;

    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }


    public function __invoke(string $context): string
    {
        $this->contextName = ucfirst(strtolower($context));

        // check if exists app folder and context path
        $this->checkPath();

        // create context folder
        $this->filesystem->mkdir(
            [
                $this->contextPath,
                $this->contextPath . '/Application/Service',
                $this->contextPath . '/Domain/Exception',
                $this->contextPath . '/Domain/Model',
                $this->contextPath . '/Domain/Repository',
                $this->contextPath . '/Infrastructure/Action',
                $this->contextPath . '/Infrastructure/Controller',
                $this->contextPath . '/Infrastructure/Gui/Twig',
                $this->contextPath . '/Infrastructure/Storage/Doctrine/Mapping',
                $this->contextPath . '/Infrastructure/Storage/Doctrine/Repository',
                $this->contextPath . '/Infrastructure/Storage/Memory/Repository',
            ]
        );

        return $this->contextPath;

    }

    /**
     * @return void
     */
    private function checkPath()
    {

        // if is test
        if (!array_key_exists('APP_ENV', $_ENV)) {

            $this->rootApp = realpath(__DIR__ . '/../../../var');


        } else {
            $this->rootApp = realpath(__DIR__ . '/../../../../../app');
        }



        // check if exists context path
        $this->checkContextPath();

    }



    /**
     * @return void
     */
    private function checkContextPath(): void
    {

        $this->contextPath = $this->rootApp . '/' . $this->contextName;

        if (is_dir($this->contextPath)) {
            throw new HexagonContextExistsException($this->contextPath);
        }


    }
}