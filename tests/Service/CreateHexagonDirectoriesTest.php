<?php

namespace Service;

use Insidesuki\Bundle\HexagonBundle\Exception\HexagonContextExistsException;
use Insidesuki\Bundle\HexagonBundle\Service\Structure\CreateHexagonStructure;
use Insidesuki\Bundle\HexagonBundle\Service\Structure\RemoveHexagonStructure;
use PHPUnit\Framework\TestCase;

class CreateHexagonDirectoriesTest extends TestCase
{

    public function testContextStructureWasCreated()
    {

        $createDirs = new CreateHexagonStructure();
        $contextDir = $createDirs('ProductoT1');

        $this->assertDirectoryExists($contextDir);
        // check all directories
        $this->assertDirectoryExists($contextDir . '/Application/Service');
        $this->assertDirectoryExists($contextDir . '/Domain/Exception');
        $this->assertDirectoryExists($contextDir . '/Domain/Model');
        $this->assertDirectoryExists($contextDir . '/Domain/Exception');
        $this->assertDirectoryExists($contextDir . '/Infrastructure/Action');
        $this->assertDirectoryExists($contextDir . '/Infrastructure/Controller');
        $this->assertDirectoryExists($contextDir . '/Infrastructure/Gui/Twig');
        $this->assertDirectoryExists($contextDir . '/Infrastructure/Storage/Doctrine/Mapping');
        $this->assertDirectoryExists($contextDir . '/Infrastructure/Storage/Doctrine/Repository');
        $this->assertDirectoryExists($contextDir . '/Infrastructure/Storage/Memory/Repository');

        // remove
        $removeStructure = new RemoveHexagonStructure();
        $removeStructure($contextDir);

        $this->assertDirectoryDoesNotExist($contextDir);


    }

    public function testIfFailWhenExistsContext()
    {


        $this->expectException(HexagonContextExistsException::class);
        $createProdExists = new CreateHexagonStructure();
        $d = $createProdExists('Hexagontest');




    }

}
