<?php

namespace Service;

use Insidesuki\Bundle\HexagonBundle\Exception\HexagonContextDoesNotExistsException;
use Insidesuki\Bundle\HexagonBundle\Service\Structure\CreateHexagonStructure;
use Insidesuki\Bundle\HexagonBundle\Service\Structure\RemoveHexagonStructure;
use PHPUnit\Framework\TestCase;

class RemoveHexagonStructureTest extends TestCase
{

    public function testFailWhenDontExistsContext()
    {
        $this->expectException(HexagonContextDoesNotExistsException::class);
        $remove = new RemoveHexagonStructure();
        $remove('dontexists');
    }

    public function testIfContextWasRemoved()
    {

        // first create
        $create = new CreateHexagonStructure();
        $context = $create('tempfolder');

        $remove = new RemoveHexagonStructure();
        $remove($context);


        $this->assertDirectoryDoesNotExist($context);

    }

}
